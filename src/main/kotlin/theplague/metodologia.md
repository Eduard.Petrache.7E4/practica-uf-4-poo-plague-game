# Metodología de entrega

## Autor
- Eduard Nicolae Petrache

## Aspectos que funcionan
- El movimiento del jugador funciona correctamente.
- El jugador puede recoger objetos y matar a los enemigos.
- Los aspectos implementados estan hechos literalmente como pedía el ejercicio.

## Aspectos que se pueden mejorar
- Se pueden implementar las funciones de reproducir y expandir.

## Puntos no desarrollados
- No se han desarrollado las funciones de reproducir y expandir.
- Al no estar desarrollado el expand no se acaba el juego como es debido, se acaba el juego después de 30 turnos
