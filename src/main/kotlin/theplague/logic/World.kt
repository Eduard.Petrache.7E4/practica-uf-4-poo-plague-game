package theplague.logic

import theplague.interfaces.IWorld
import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import theplague.logic.colony.Ant
import theplague.logic.colony.Drake
import theplague.logic.vehicle.Bicycle
import theplague.logic.vehicle.Helicopter
import theplague.logic.vehicle.OnFoot
import theplague.logic.vehicle.Vehicle
import theplague.logic.weapon.Broom
import theplague.logic.weapon.Hand
import theplague.logic.weapon.Sword
import theplague.logic.weapon.Weapon

class World : IWorld {

    override val width = 10
    override val height = 10
    override val territories = List(height) { y ->
        List(width) { x -> Territory(Position(x, y)) }
    }
    override val player = Player(Position(width / 2, height / 2)).apply {
        territories[position.y][position.x].hasPlayer = true
    }

    override fun canMoveTo(position: Position) =
        player.currentVehicle.canMove(player.position.copy(), position.copy()) && position != player.position

    override fun moveTo(position: Position) {
        territories[player.position.y][player.position.x].hasPlayer = false
        player.position = position.copy()
        territories[player.position.y][player.position.x].hasPlayer = true
    }

    override fun takeableItem(): Iconizable? =
        territories[player.position.y][player.position.x].item

    override fun takeItem() {
        territories[player.position.y][player.position.x].item?.let { item ->
            when (item) {
                is Weapon -> player.currentWeapon = item
                is Vehicle -> player.currentVehicle = item
            }
        }
        territories[player.position.y][player.position.x].item = null
    }

    override fun nextTurn() {
        generateNewItems()
        generateNewColonies()
        player.turns++
        checkVehicle()
    }

    private fun generateNewItems() {
        val emptyTerritories = territories.flatten().filter { it.item == null && !it.hasPlayer }
        if (emptyTerritories.isNotEmpty()) {
            val randomTerritory = emptyTerritories.random()
            val item = when ((1..100).random()) {
                in 1..25 -> Bicycle()
                in 26..50 -> Broom()
                in 51..60 -> Helicopter()
                in 61..70 -> Sword()
                else -> null
            }
            randomTerritory.item = item
        }
        territories[player.position.y][player.position.x].item = null
    }

    private fun generateNewColonies() {
        val emptyTerritories = territories.flatten().filter { it.colony == null }
        if (emptyTerritories.isNotEmpty()) {
            val randomTerritory = emptyTerritories.random()
            val colony = when ((1..100).random()) {
                in 1..30 -> Ant()
                in 31..40 -> Drake()
                else -> null
            }
            randomTerritory.colony = colony
        }
    }

    private fun checkVehicle() {
        player.currentVehicle.durability?.let { durability ->
            player.currentVehicle.durability = durability - 1
            if (durability == 1) {
                player.currentVehicle = OnFoot()
            }
        }
    }

    override fun exterminate() {
        territories[player.position.y][player.position.x].colony?.let { colony ->
            when (colony) {
                is Ant -> when (player.currentWeapon) {
                    is Hand -> colony.size -= Hand().damageAnt
                    is Sword -> colony.size -= Sword().damageAnt
                    is Broom -> colony.size -= Broom().damageAnt
                }
                is Drake -> when (player.currentWeapon) {
                    is Hand -> colony.size -= Hand().damageDrake
                    is Sword -> colony.size -= Sword().damageDrake
                    is Broom -> colony.size -= Broom().damageDrake
                }
            }
            if (colony.size <= 0) {
                territories[player.position.y][player.position.x].colony = null
            }
        }
    }

    override fun gameFinished(): Boolean {
        return player.turns == 30
    }
}

