package theplague.logic.vehicle

import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import theplague.logic.Item

abstract class Vehicle :Item(), Iconizable{
    abstract var durability :Int?
    abstract fun canMove(from: Position, to: Position): Boolean
}