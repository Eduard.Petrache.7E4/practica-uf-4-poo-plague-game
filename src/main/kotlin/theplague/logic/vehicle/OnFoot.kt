package theplague.logic.vehicle

import theplague.interfaces.Position


class OnFoot :Vehicle(){
    override val icon: String
        get() = "\uD83D\uDEB6"
    val vel: Int = 1
    override var durability: Int? = null
    override fun canMove(from: Position, to: Position): Boolean {
        val rangX = (from.x - vel) .. (from.x + vel)
        val rangY = (from.y - vel) .. (from.y + vel)
        return to.x in rangX && to.y in rangY
    }
}