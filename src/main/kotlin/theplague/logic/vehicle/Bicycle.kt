package theplague.logic.vehicle

import theplague.interfaces.Position

class Bicycle: Vehicle() {
    override val icon: String
        get() = " \uD83D\uDEB2"
    override var durability: Int? = 6
    private val vel = 4
    override fun canMove(from: Position, to: Position): Boolean {
        val rangX = (from.x - vel) .. (from.x + vel)
        val rangY = (from.y - vel) .. (from.y + vel)
        return to.x in rangX && to.y in rangY
    }
}