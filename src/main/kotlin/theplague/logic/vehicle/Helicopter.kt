package theplague.logic.vehicle

import theplague.interfaces.Position

class Helicopter: Vehicle() {
    override val icon: String
        get() = "\uD83D\uDE81"
    override var durability: Int? = 6
    override fun canMove(from: Position, to: Position): Boolean {
        return true
    }
}