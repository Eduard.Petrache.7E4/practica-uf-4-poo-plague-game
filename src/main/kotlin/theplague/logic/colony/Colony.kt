package theplague.logic.colony

import theplague.interfaces.Iconizable

sealed class Colony: Iconizable {
    var size:Int = 1
}