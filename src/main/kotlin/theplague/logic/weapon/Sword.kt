package theplague.logic.weapon

class Sword :Weapon(){
    override val icon: String
        get() = "\uD83D\uDDE1"
    override val damageAnt: Int = 1
    override val damageDrake: Int = 1
}