package theplague.logic.weapon

import theplague.interfaces.Iconizable
import theplague.logic.Item

abstract class Weapon : Iconizable, Item(){
    abstract val damageAnt: Int
    abstract val damageDrake: Int
}