package theplague.logic.weapon

class Hand: Weapon() {
    override val icon: String
        get() = " \uD83D\uDC46"
    override val damageAnt = 1
    override val damageDrake = 0
}