package theplague.logic.weapon

class Broom : Weapon() {
    override val icon: String
        get() = "\uD83E\uDDF9"
    override val damageAnt: Int = 0
    override val damageDrake: Int = 0
}