package theplague.logic

import theplague.interfaces.ITerritory
import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import theplague.logic.colony.Colony

class Territory(var position: Position): ITerritory {
    var hasPlayer = false
    var item:Item? = null
    var colony: Colony? = null
    override fun iconList(): List<Iconizable> {
        val llista = mutableListOf<Iconizable>()
        if (hasPlayer){
            llista.add(Player())
        }
        if (item != null){
            llista.add(item!!)
        }
        if (colony != null){
            repeat(colony!!.size) {
                llista.add(colony!!)
            }
        }

        return llista
    }

}