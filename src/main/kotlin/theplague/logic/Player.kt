package theplague.logic

import theplague.interfaces.IPlayer
import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import theplague.logic.vehicle.OnFoot
import theplague.logic.vehicle.Vehicle
import theplague.logic.weapon.Hand
import theplague.logic.weapon.Weapon

class Player(initialPosition: Position = Position(0, 0)): IPlayer, Iconizable {
    override var turns: Int = 0
    override var livesLeft: Int = 15
    var position: Position = initialPosition
    override val icon: String
        get() = "\uD83D\uDEB6"
    override var currentVehicle: Vehicle = OnFoot()
    override var currentWeapon: Weapon = Hand()
}