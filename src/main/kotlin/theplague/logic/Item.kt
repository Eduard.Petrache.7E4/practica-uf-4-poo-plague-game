package theplague.logic

import theplague.interfaces.Iconizable

abstract class Item: Iconizable {
    fun use() {
    }
}